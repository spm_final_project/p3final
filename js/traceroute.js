// Reference Source:  http://stackoverflow.com/questions/7165395/call-php-function-from-javascript
var validFlag;
var container;
var url;

function createTraceroute() {
	url = document.getElementById('fbase').value;
	container = document.getElementById('output');
//	validFlag = 0;
	checkValid(url);
	getTraceroute(url);
	container.innerHTML = '<img src="images/processing_sm.gif">'

	return false;
}

function checkValid(url) {
	container.innerHTML = '<img src="images/validating_sm.gif">'
	var phpRequest = 'validate.php?url='+url;
	getRequest(
		phpRequest,	// URL for the PHP file
	   urlValid,	// handle successful request
		reqError		// handle error
	);
	return false;
}

// This could probably use some DRY attention
function getTraceroute(url) {
	container.innerHTML = '<img src="images/processing_sm.gif">'
	var phpRequest = 't-route.php?url='+url;
	getRequest(
		phpRequest, 			// URL for the PHP file
   	processTraceroute,  	// handle successful request
		reqError    			// handle error
	);
	return false;
}

// handles the response, adds the html
function urlValid(responseText) {
	var container = document.getElementById('output');
	container.innerHTML = responseText;

	//getTraceroute(url);
}

// handles the response, adds the html
function processTraceroute(responseText) {
	var container = document.getElementById('output');
	var traceroute = JSON.parse(responseText);
	makeMagic(traceroute);
	//container.innerHTML = responseText;
}

// handles drawing an error message
function reqError() {
	var container = document.getElementById('output');
	container.innerHTML = 'Bummer: there was an error!';
}

// helper function for cross-browser request object
function getRequest(url, success, error) {
	var req = false;
	try {
		// most browsers
		req = new XMLHttpRequest();
	} catch (e) {
		// IE
		try {
			req = new ActiveXObject("Msxml2.XMLHTTP");
		} catch(e) {
			// try an older version
			try {
				req = new ActiveXObject("Microsoft.XMLHTTP");
			} catch(e) {
				return false;
			}
		}
	}
	if (!req) return false;
//	if (typeof success != 'function') success = function () {};
//	if (typeof error != 'function') error = function () {};
	req.onreadystatechange = function() {
		if(req.readyState == 4) {
//			return req.status === 200 ? 
//            success(req.responseText) : error(req.status);
			if(req.status === 200) {
				success(req.responseText);
			} else {
				error(req.status);
			} 
		}
	}
	req.open("GET", url, true);
	req.send(null);
return req;
}
