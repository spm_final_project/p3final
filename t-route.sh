#!/bin/bash

rm tracefile.txt

sudo traceroute -nI $1 | sed s/"*"//g | grep -v traceroute | awk '{print $2}' | grep -v -e '^$'  > tracefile.txt

rm troutput.txt

echo "[" >> troutput.txt

count=0;
while read p; do
   if ! [ $count -eq 0 ]; then
      echo "," >> troutput.txt
   fi
   count=$(($count+1))
   curl -s http://ip-api.com/json/$p >> troutput.txt
done <tracefile.txt

echo >> troutput.txt
echo "]" >> troutput.txt

awk '{print}' troutput.txt
